# JavaFX Artillery Game

JavaFX game made in the first year of Applied Informatics at Karel de Grote Hogeschool.

## Description

The project is made using the MVP pattern with Java. The game is an artillery game
where you pick an angle and a speed between 0-10 and shoot your projectile, in hopes
of hitting your enemy at the other side of the hill.

The game features:
* Custom Graphics
* Animations
* Projectile deviation based on wind direction & speed
* Terrain Deformation
* Multiple Biomes
* Local Leaderboard