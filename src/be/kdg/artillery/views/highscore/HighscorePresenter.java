package be.kdg.artillery.views.highscore;

import be.kdg.artillery.model.Artillery;
import be.kdg.artillery.model.Player;
import be.kdg.artillery.views.home.HomePresenter;
import be.kdg.artillery.views.home.HomeView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class HighscorePresenter {
    private HighscoreView view;
    private Artillery model;

    public HighscorePresenter(Artillery model, HighscoreView highscoreView) {
        this.view = highscoreView;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getBtnMainMenu().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                HomeView homeView = new HomeView();
                Artillery model = new Artillery();
                HomePresenter presenter = new HomePresenter(homeView, model);
                view.getScene().setRoot(homeView);
            }
        });
    }

    private void updateView() {
        /*Loads all the players into a Playerlist, then prints a line for the 1st player in that list: "1. Maarten 12".
        then remove the 1st player so a new player takes the number 1 spot. repeat these steps 10 times until only the
         top 10 players get printed*/
        model.getPlayerManager().load();
        StringBuilder sb = new StringBuilder();
        int teller = 1;

        if (model.getPlayerManager().getPlayerList().size() > 10){
            for (int i = 0; i < 10; i++) {
                sb.append(teller + ". " + model.getPlayerManager().getPlayerList().get(0).getName() + " " + model.getPlayerManager().getPlayerList().get(0).getTurns() + "\n");
                model.getPlayerManager().getPlayerList().remove(0);
                teller++;
            }
        } else {
            for (Player player : model.getPlayerManager().getPlayerList()) {
                sb.append(teller + ". " + player.getName() + " " + player.getTurns() + "\n");
                teller++;
            }
        }
        view.getLblHighscores().setText(sb.toString());
    }
}