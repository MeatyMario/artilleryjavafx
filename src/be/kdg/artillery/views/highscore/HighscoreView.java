package be.kdg.artillery.views.highscore;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class HighscoreView extends VBox {
    private Label lblArtillery;
    private Button btnMainMenu;
    private VBox vBox;
    private Label lblHighscores;

    public HighscoreView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        lblArtillery = new Label("Artillery");
        btnMainMenu = new Button("Main Menu");
        lblHighscores = new Label();
        vBox = new VBox();
    }

    private void layoutNodes() {
        this.setSpacing(4);
        this.setAlignment(Pos.CENTER);

        vBox.setMaxWidth(300);
        vBox.setMinHeight(200);
        vBox.setSpacing(20);
        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(20));
        vBox.setId("artilleryVBox");
        lblHighscores.setId("highscoreLbl");
        btnMainMenu.setId("artilleryMenuButton");
        vBox.getChildren().addAll(lblHighscores, btnMainMenu);

        lblArtillery.setId("artilleryTitle");

        this.getChildren().addAll(lblArtillery, vBox);

        BackgroundImage myBI = new BackgroundImage(new Image("homebackground.jpg", 1600, 900, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.setBackground(new Background(myBI));
    }

    Label getLblHighscores() {
        return lblHighscores;
    }
    Button getBtnMainMenu() {
        return btnMainMenu;
    }
}
