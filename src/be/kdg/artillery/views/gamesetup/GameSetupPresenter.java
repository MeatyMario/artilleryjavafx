package be.kdg.artillery.views.gamesetup;

import be.kdg.artillery.model.Artillery;
import be.kdg.artillery.views.game.GamePresenter;
import be.kdg.artillery.views.game.GameView;
import be.kdg.artillery.views.home.HomePresenter;
import be.kdg.artillery.views.home.HomeView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class GameSetupPresenter {
    private GameSetupView view;
    private Artillery model;

    public GameSetupPresenter(Artillery model, GameSetupView gameSetupView) {
        this.view = gameSetupView;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getBtnFight().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (view.getTfPlayer1().getText() == null || !view.getTfPlayer1().getText().matches("^[a-zA-Z]*$") ||
                        view.getTfPlayer1().getText().length() > 8 || (view.getTfPlayer1().getText().trim().isEmpty()) ||
                        view.getTfPlayer2().getText() == null || view.getTfPlayer2().getText().length() > 8 ||
                        !view.getTfPlayer2().getText().matches("^[a-zA-Z]*$") || (view.getTfPlayer2().getText().trim().isEmpty())) {
                    Alert alert= new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Unable to Continue");
                    alert.setContentText("Please give a text-based name, not longer than 8 characters.");
                    alert.showAndWait();
                } else {
                    model.getPlayerManager().createPlayers(view.getTfPlayer1().getText(), view.getTfPlayer2().getText()); //create new players
                    Artillery artillery = new Artillery();
                    GameView gameView = new GameView();
                    GamePresenter gamePresenter = new GamePresenter(artillery, gameView);
                    view.getScene().setRoot(gameView);
                }
            }
        });

        view.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    HomeView homeView = new HomeView();
                    Artillery model = new Artillery();
                    HomePresenter presenter = new HomePresenter(homeView, model);
                    view.getScene().setRoot(homeView);
                }
            }
        });
    }

    private void updateView() {
    }
}
