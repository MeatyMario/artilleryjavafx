package be.kdg.artillery.views.gamesetup;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class GameSetupView extends VBox {
    private Label lblArtillery;
    private Label lblPlayer1, lblPlayer2;
    private TextField tfPlayer1, tfPlayer2;
    private Button btnFight;

    public GameSetupView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        lblArtillery = new Label("Artillery");
        lblPlayer1 = new Label("Player 1 Name:");
        tfPlayer1 = new TextField();
        lblPlayer2 = new Label("Player 2 Name:");
        tfPlayer2 = new TextField();
        btnFight = new Button("Fight!");
    }

    private void layoutNodes() {
        lblArtillery.setId("artilleryTitle");
        super.setAlignment(Pos.CENTER);
        super.setSpacing(20);
        VBox vbox = new VBox();
        vbox.setMaxWidth(300);
        vbox.setMinHeight(200);
        vbox.setSpacing(20);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(20));
        vbox.setId("artilleryVBox");
        lblPlayer1.setId("gamesetupViewLblPlayer");
        lblPlayer2.setId("gamesetupViewLblPlayer");
        btnFight.setId("artilleryMenuButton");
        vbox.getChildren().addAll(lblPlayer1, tfPlayer1, lblPlayer2, tfPlayer2, btnFight);
        super.getChildren().addAll(lblArtillery, vbox);
        BackgroundImage myBI = new BackgroundImage(new Image("homebackground.jpg", 1600, 900, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.setBackground(new Background(myBI));
    }

     TextField getTfPlayer1() {
        return tfPlayer1;
    }

     TextField getTfPlayer2() {
        return tfPlayer2;
    }

     Button getBtnFight() {
        return btnFight;
    }
}
