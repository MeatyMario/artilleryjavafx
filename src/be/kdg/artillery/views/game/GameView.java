package be.kdg.artillery.views.game;

import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;

/* Made by Thomas Van Sande & Mario Mersini */
public class GameView extends Pane {
    //Controlpanel Attributes
    private HBox hboxControlPanel;
    private VBox vboxAngle, vboxPower;
    private Slider sldrAngle;
    private Label lblAngle;
    private Label lblTextAngle;
    private Slider sldrPower;
    private Label lblPower;
    private Label lblTextPower;
    private Button btnShoot;
    private Label lblWindDir, lblPlayerTurn;
    private Label lblScorePl1, lblScorePl2;

    //Victoryscreen Attributes
    private VBox vboxVictory;
    private Label lblWin, lblName;
    private Button btnMainMenu, btnNextRound;

    //Game Attributes
    private Polygon pgnTerrain;
    private Circle crclProjectile;
    private ImageView ivBluHead, ivBluBody;
    private ImageView ivLedHead, ivLedBody;
    private ImageView ivCloud1, ivCloud2;
    private ImageView ivWindFlag;
    private ImageView ivSnow;
    private Circle crclExplosion;

    public GameView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        //Uses a JavaFX Polygon shape to create the terrain.
        pgnTerrain = new Polygon();

        //Grey controlbar at the bottom containing all the nodes.
        hboxControlPanel = new HBox();
        vboxAngle = new VBox();
        vboxPower = new VBox();
        sldrAngle = new Slider(0, 90, 45);
        sldrPower = new Slider(0, 10, 5);
        lblAngle = new Label();
        lblPower = new Label();
        lblTextAngle = new Label("Angle");
        lblTextPower = new Label("Power");
        lblAngle.textProperty().bind(
                Bindings.format(
                        "%.0f",
                        sldrAngle.valueProperty()
                )
        );
        lblPower.textProperty().bind(
                Bindings.format(
                        "%.0f",
                        sldrPower.valueProperty()
                )
        );
        btnShoot = new Button("FIRE");
        lblWindDir = new Label();
        lblPlayerTurn = new Label();
        lblScorePl1 = new Label();
        lblScorePl2 = new Label();

        //Victory endscreen
        lblWin = new Label("Victory");
        lblName = new Label();
        btnMainMenu = new Button("Main Menu");
        btnNextRound = new Button("Next Round");
        vboxVictory = new VBox(lblWin, lblName, btnNextRound, btnMainMenu);

        //Projectile + explosion
        crclProjectile = new Circle(5);
        crclExplosion = new Circle(10, Color.ORANGE);

        //Blu and Led
        ivBluHead = new ImageView(new Image("/bluhead.png"));
        ivBluBody = new ImageView(new Image("/blubody.png"));
        ivLedHead = new ImageView(new Image("/ledhead.png"));
        ivLedBody = new ImageView(new Image("/ledbody.png"));

        //Windflag at the top right and clouds
        ivWindFlag = new ImageView(new Image("/windflag.png"));
        ivCloud1 = new ImageView(new Image("/cloud1.png"));
        ivCloud2 = new ImageView(new Image("/cloud2.png"));

        //ImageView containing snow with transparent background for the snow biome
        ivSnow = new ImageView(new Image("/snow2.png"));
    }

    private void layoutNodes() {
        //Terrain settings. Default colors set and the layout, aswel as adding it onto the Pane.
        pgnTerrain.setFill(Color.web("#458448"));
        pgnTerrain.setStroke(Color.DARKGREEN);
        pgnTerrain.setStrokeWidth(4);
        this.getChildren().add(pgnTerrain);

        //Lay-out settings for the wind flag and adding it in the game.
        ivWindFlag.setX(1200);
        this.getChildren().add(ivWindFlag);

        //Lay-out settings for the clouds
        ivCloud1.setX(-700);
        ivCloud1.setY(-50);
        ivCloud2.setX(-300);
        ivCloud2.setY(50);
        this.getChildren().add(0, ivCloud1);
        this.getChildren().add(0, ivCloud2);

        //Lay-out settings for the projectile.
        crclProjectile.setFill(Color.BLACK);

        //Lay-out settings for the tanks and adding it on the screen.
        ivBluHead.setFitHeight(100);
        ivBluHead.setFitWidth(100);
        ivBluBody.setFitHeight(100);
        ivBluBody.setFitWidth(100);
        this.getChildren().add(0, ivBluHead);
        this.getChildren().add(0, ivBluBody);
        ivLedHead.setFitHeight(100);
        ivLedHead.setFitWidth(100);
        ivLedBody.setFitHeight(100);
        ivLedBody.setFitWidth(100);
        this.getChildren().add(0, ivLedHead);
        this.getChildren().add(0, ivLedBody);

        //Adding AngleSlider and the Angle Labels to the vboxAngle.
        sldrAngle.setMinWidth(250);
        sldrAngle.setId("gameViewSlider");
        lblAngle.setId("gameViewLbl");
        lblTextAngle.setId("gameViewSmallLbl");
        vboxAngle.setAlignment(Pos.TOP_CENTER);
        vboxAngle.getChildren().addAll(lblAngle, sldrAngle, lblTextAngle);

        //Adding PowerSlider and the Power Labels to the vboxPower.
        sldrPower.setMinWidth(250);
        sldrPower.setId("gameViewSlider");
        lblPower.setId("gameViewLbl");
        lblTextPower.setId("gameViewSmallLbl");
        vboxPower.setAlignment(Pos.TOP_CENTER);
        vboxPower.getChildren().addAll(lblPower, sldrPower, lblTextPower);

        //Lay-out settings for the Shoot Button.
        double r = 40;
        btnShoot.setShape(new Circle(r));
        btnShoot.setPrefSize(2 * r, 2 * r);
        btnShoot.setTranslateY(-20);
        btnShoot.setTranslateX(-30);
        btnShoot.setId("gameViewFireBtn");

        //Adding the Labels containing player round scores to the vboxScore.
        VBox vboxScore = new VBox();
        Label lblPlayer1 = new Label("Player 1:");
        Label lblPlayer2 = new Label("Player 2:");
        lblPlayer1.setId("gameViewSmallLbl");
        lblPlayer2.setId("gameViewSmallLbl");
        lblScorePl1.setId("gameViewDataControlPanel");
        lblScorePl2.setId("gameViewDataControlPanel");
        lblScorePl1.setTranslateY(-5);
        lblPlayer2.setTranslateY(-5);
        lblScorePl2.setTranslateY(-10);
        vboxScore.getChildren().addAll(lblPlayer1, lblScorePl1, lblPlayer2, lblScorePl2);

        //Adding the Labels containing game information to the vboxDataControlPanel
        VBox vboxDataControlPanel = new VBox();
        Label lblCurrentPlayer = new Label ("Current Player:");
        Label lblWindSpeed = new Label ("Windspeed: ");
        lblCurrentPlayer.setId("gameViewSmallLbl");
        lblWindSpeed.setId("gameViewSmallLbl");
        lblPlayerTurn.setId("gameViewDataControlPanel");
        lblWindDir.setId("gameViewDataControlPanel");
        lblWindDir.setTranslateY(-10);
        lblWindSpeed.setTranslateY(-5);
        lblPlayerTurn.setTranslateY(-5);
        vboxDataControlPanel.getChildren().addAll(lblCurrentPlayer, lblPlayerTurn, lblWindSpeed, lblWindDir);

        //The hbox which is the control panel. Lay-out settings and adding all above nodes to it and adding onto pane.
        hboxControlPanel.setId("artilleryVBox");
        hboxControlPanel.setLayoutY(650);
        hboxControlPanel.setPrefHeight(150);
        hboxControlPanel.setPrefWidth(1400);
        hboxControlPanel.setSpacing(40);
        hboxControlPanel.setAlignment(Pos.CENTER_RIGHT);
        hboxControlPanel.getChildren().addAll(vboxScore, vboxDataControlPanel, vboxAngle, vboxPower, btnShoot);
        this.getChildren().add(hboxControlPanel);

        // The victory screens that gets showed at the end of round wins and game wins.
        vboxVictory.setMinWidth(400);
        vboxVictory.setMaxWidth(400);
        vboxVictory.setLayoutX(500);
        vboxVictory.setLayoutY(180);
        vboxVictory.setAlignment(Pos.CENTER);
        vboxVictory.setSpacing(25);
        vboxVictory.setMinHeight(200);
        vboxVictory.setPadding(new Insets(20));
        vboxVictory.setId("artilleryVBox");
        lblWin.setId("gameViewVictory");
        lblName.setId("gameViewWinnerName");
        btnNextRound.setId("artilleryMenuButton");
        btnMainMenu.setId("artilleryMenuButton");


        //Adding the background image.
        BackgroundImage myBI = new BackgroundImage(new Image("background.jpg", 1600, 900, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.setBackground(new Background(myBI));
    }

    Circle getCrclExplosion() {
        return crclExplosion;
    }

    ImageView getIvSnow() {
        return ivSnow;
    }

    Label getLblScorePl1() {
        return lblScorePl1;
    }

    Label getLblScorePl2() {
        return lblScorePl2;
    }

    ImageView getIvCloud1() {
        return ivCloud1;
    }

    ImageView getIvCloud2() {
        return ivCloud2;
    }

    Label getLblPlayerTurn() {
        return lblPlayerTurn;
    }

    Button getBtnMainMenu() {
        return btnMainMenu;
    }

    Button getBtnNextRound() {
        return btnNextRound;
    }

    VBox getVboxVictory() {
        return vboxVictory;
    }

    Label getLblName() {
        return lblName;
    }

    ImageView getIvWindFlag() {
        return ivWindFlag;
    }

    Label getLblWindDir() {
        return lblWindDir;
    }

    ImageView getIvBluHead() {
        return ivBluHead;
    }

    ImageView getIvBluBody() {
        return ivBluBody;
    }

    ImageView getIvLedHead() {
        return ivLedHead;
    }

    ImageView getIvLedBody() {
        return ivLedBody;
    }

    Circle getCrclProjectile() {
        return crclProjectile;
    }

    Slider getSldrAngle() {
        return sldrAngle;
    }

    Slider getSldrPower() {
        return sldrPower;
    }

    Button getBtnShoot() {
        return btnShoot;
    }

    Polygon getPgnTerrain() {
        return pgnTerrain;
    }
}
