package be.kdg.artillery.views.game;

import be.kdg.artillery.model.Artillery;
import be.kdg.artillery.views.home.HomePresenter;
import be.kdg.artillery.views.home.HomeView;
import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class GamePresenter {
    private GameView view;
    private Artillery model;
    private AnimationTimer atShotFired;


    public GamePresenter(Artillery artillery, GameView gameView) {
        this.view = gameView;
        this.model = artillery;
        addEventHandlers();
        updateView();
        drawWind();
        drawTerrain();
        handleBiome();
        startCloudAnimation();
    }

    //This method is responsible for the cloud animation when a game is started.
    private void startCloudAnimation() {
        if (model.getProjectile().getWind().getwX() > 0) {
            view.getIvCloud1().setX(1700);
            view.getIvCloud2().setX(1500);

            TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(25), view.getIvCloud1());
            translateTransition1.setByX(-2200);
            translateTransition1.setCycleCount(Animation.INDEFINITE);
            translateTransition1.play();

            TranslateTransition translateTransition2 = new TranslateTransition(Duration.seconds(20), view.getIvCloud2());
            translateTransition2.setByX(-2000);
            translateTransition2.setCycleCount(Animation.INDEFINITE);
            translateTransition2.play();
        } else {
            TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(25), view.getIvCloud1());
            translateTransition1.setByX(2200);
            translateTransition1.setCycleCount(Animation.INDEFINITE);
            translateTransition1.play();

            TranslateTransition translateTransition2 = new TranslateTransition(Duration.seconds(20), view.getIvCloud2());
            translateTransition2.setByX(1700);
            translateTransition2.setCycleCount(Animation.INDEFINITE);
            translateTransition2.play();
        }
    }

    //This method is responsible for setting the wind in the gameView from the model.
    private void drawWind() {
        double windPower = model.getProjectile().getWind().getwX(); //Creates a variable to make the code more readable.

        String windSpeed = Math.round(Math.abs(windPower) * 1000) + " Knots";
        if (windPower > 0) {
            view.getLblWindDir().setText(windSpeed);
        } else if (windPower < 0) {
            view.getLblWindDir().setText(windSpeed);
        }
    }

    //This method is responsible for drawing the terrain.
    private void drawTerrain() {
        /* In the following 2 For loops we first delete the terrain, this is needed if the terrain is already drawn and we need to refresh to create
           terrain deformation. Then we draw the terrain again. */
        for (int i = 0; i < model.getTerrain().getTerrainCoordinates().size(); i++) {
            view.getPgnTerrain().getPoints().removeAll(model.getTerrain().getTerrainCoordinates().get(i).getX(), model.getTerrain().getTerrainCoordinates().get(i).getY());
        }
        view.getPgnTerrain().getPoints().removeAll(1400.0, 1000.0, 0.0, 1000.0);
        for (int i = 0; i < model.getTerrain().getTerrainCoordinates().size(); i++) {
            view.getPgnTerrain().getPoints().addAll(model.getTerrain().getTerrainCoordinates().get(i).getX(), model.getTerrain().getTerrainCoordinates().get(i).getY());
        }
        view.getPgnTerrain().getPoints().addAll(1400.0, 1000.0, 0.0, 1000.0);
    }

    //This method is the move method, it contains the game loop and gets initiated when a shot is fired.
    private void move() {
        atShotFired = new AnimationTimer() {
            public void handle(long now) {
                model.checkCollisions();
                handleCollisions();
                view.getCrclProjectile().setLayoutX(model.getProjectile().getX());
                view.getCrclProjectile().setLayoutY(model.getProjectile().getY());
            }
        };
    }

    //This method handles the different biomes correctly with information from the model. For each biome it changes some stuff.
    private void handleBiome() {
        model.getTerrain().generateBiome();
        switch (model.getTerrain().getBiome()) {
            case 0:
                view.getPgnTerrain().setFill(Color.web("#458448"));
                view.getPgnTerrain().setStroke(Color.DARKGREEN);
                break;
            case 1:
                view.getPgnTerrain().setFill(Color.web("#FAB371"));
                view.getPgnTerrain().setStroke(Color.web("F79E54"));
                view.getIvBluHead().setImage(new Image("bluheadcowboy.png"));
                view.getIvLedHead().setImage(new Image("ledheadcowboy.png"));
                BackgroundImage desert = new BackgroundImage(new Image("desertbackground.png", 1600, 900, false, true),
                        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                        BackgroundSize.DEFAULT);
                view.setBackground(new Background(desert));
                break;
            case 2:
                view.getPgnTerrain().setFill(Color.web("#E9EEFF"));
                view.getPgnTerrain().setStroke(Color.web("BDC7E5"));
                view.getIvBluHead().setImage(new Image("bluheadsanta.png"));
                view.getIvLedHead().setImage(new Image("ledheadsanta.png"));
                view.getChildren().add(6, view.getIvSnow());
                view.getIvSnow().setY(-3500);
                view.getIvSnow().setX(-100);
                view.getChildren().removeAll(view.getIvCloud1(), view.getIvCloud2());
                TranslateTransition moveSnow = new TranslateTransition(Duration.seconds(240), view.getIvSnow());
                moveSnow.setCycleCount(Animation.INDEFINITE);
                moveSnow.setToY(4600);
                moveSnow.setInterpolator(Interpolator.LINEAR);
                moveSnow.play();
                BackgroundImage snow = new BackgroundImage(new Image("snowybackground.png", 1600, 900, false, true),
                        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                        BackgroundSize.DEFAULT);
                view.setBackground(new Background(snow));
                break;
        }
    }

    //Specific animations for the characters when the Blu player wins.
    private void bluWinAnimation() {
        RotateTransition rotateBluHead = new RotateTransition(Duration.millis(400), view.getIvBluHead());
        rotateBluHead.setToAngle(30);
        rotateBluHead.setCycleCount(Animation.INDEFINITE);
        rotateBluHead.setAutoReverse(true);
        rotateBluHead.play();

        RotateTransition rotateBluBody = new RotateTransition(Duration.millis(400), view.getIvBluBody());
        rotateBluBody.setByAngle(-10);
        rotateBluBody.setCycleCount(Animation.INDEFINITE);
        rotateBluBody.setAutoReverse(true);
        rotateBluBody.play();

        TranslateTransition jumpBluBody = new TranslateTransition(Duration.millis(400), view.getIvBluBody());
        jumpBluBody.setByY(-40);
        jumpBluBody.setCycleCount(Animation.INDEFINITE);
        jumpBluBody.setAutoReverse(true);
        jumpBluBody.play();

        TranslateTransition jumpBluHead = new TranslateTransition(Duration.millis(400), view.getIvBluHead());
        jumpBluHead.setByY(-40);
        jumpBluHead.setCycleCount(Animation.INDEFINITE);
        jumpBluHead.setAutoReverse(true);
        jumpBluHead.play();

        RotateTransition rotateLedBody = new RotateTransition(Duration.millis(400), view.getIvLedBody());
        rotateLedBody.setToAngle(90);
        rotateLedBody.play();

        RotateTransition rotateLedHead = new RotateTransition(Duration.millis(1000), view.getIvLedHead());
        rotateLedHead.setToAngle(360);
        rotateLedHead.setCycleCount(Animation.INDEFINITE);
        rotateLedHead.play();

        TranslateTransition moveLedBody = new TranslateTransition(Duration.millis(400), view.getIvLedBody());
        moveLedBody.setByX(40);
        moveLedBody.setByY(20);
        moveLedBody.play();

        TranslateTransition flyLedHead = new TranslateTransition(Duration.seconds(1), view.getIvLedHead());
        flyLedHead.setByX(200);
        flyLedHead.setByY(-50);
        flyLedHead.play();
    }

    //Specific animations for the characters when the Led player wins.
    private void ledWinAnimation() {
        RotateTransition rotateLedHead = new RotateTransition(Duration.millis(400), view.getIvLedHead());
        rotateLedHead.setToAngle(-30);
        rotateLedHead.setCycleCount(Animation.INDEFINITE);
        rotateLedHead.setAutoReverse(true);
        rotateLedHead.play();

        RotateTransition rotateLedBody = new RotateTransition(Duration.millis(400), view.getIvLedBody());
        rotateLedBody.setByAngle(10);
        rotateLedBody.setCycleCount(Animation.INDEFINITE);
        rotateLedBody.setAutoReverse(true);
        rotateLedBody.play();

        TranslateTransition jumpLedBody = new TranslateTransition(Duration.millis(400), view.getIvLedBody());
        jumpLedBody.setByY(-40);
        jumpLedBody.setCycleCount(Animation.INDEFINITE);
        jumpLedBody.setAutoReverse(true);
        jumpLedBody.play();

        TranslateTransition jumpLedHead = new TranslateTransition(Duration.millis(400), view.getIvLedHead());
        jumpLedHead.setByY(-40);
        jumpLedHead.setCycleCount(Animation.INDEFINITE);
        jumpLedHead.setAutoReverse(true);
        jumpLedHead.play();

        RotateTransition rotateBluBody = new RotateTransition(Duration.millis(400), view.getIvBluBody());
        rotateBluBody.setToAngle(-90);
        rotateBluBody.play();

        RotateTransition rotateBluHead = new RotateTransition(Duration.millis(1000), view.getIvBluHead());
        rotateBluHead.setToAngle(-360);
        rotateBluHead.setCycleCount(Animation.INDEFINITE);
        rotateBluHead.play();

        TranslateTransition moveBluBody = new TranslateTransition(Duration.millis(400), view.getIvBluBody());
        moveBluBody.setByX(-40);
        moveBluBody.setByY(20);
        moveBluBody.play();

        TranslateTransition flyBluHead = new TranslateTransition(Duration.seconds(1), view.getIvBluHead());
        flyBluHead.setByX(-200);
        flyBluHead.setByY(-50);
        flyBluHead.play();
    }

    //Handles the projectile explosion when hitting terrain, aswel as deleting it from view when done.
    private void projectileExplosion() {
        ScaleTransition stExplosion = new ScaleTransition(Duration.millis(200), view.getCrclExplosion());
        view.getChildren().add(view.getCrclExplosion());
        view.getCrclExplosion().setLayoutX(model.getProjectile().getX());
        view.getCrclExplosion().setLayoutY(model.getProjectile().getY());
        stExplosion.setFromX(2);
        stExplosion.setFromY(2);
        stExplosion.playFromStart();

        stExplosion.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                stExplosion.stop();
                view.getChildren().remove(view.getCrclExplosion());
            }
        });
    }

    //This method handles collisions when one is detected from the model class.
    private void handleCollisions() {
        //Terrain collision detected.
        if (model.isTerrainCollisionDetected()) {
            view.getChildren().remove(view.getCrclProjectile());
            view.getBtnShoot().setDisable(false);
            model.setTerrainCollisionDetected(false);
            model.setTankCollisionDetected(false);
            drawTerrain();
            projectileExplosion();
            atShotFired.stop();
            if (model.isPlayer1Turn()) {
                model.setPlayer1Turn(false);
                view.getLblPlayerTurn().setText(model.getPlayerManager().getPlayer2().getName()); //set playerturn name
            } else {
                model.setPlayer1Turn(true);
                view.getLblPlayerTurn().setText(model.getPlayerManager().getPlayer1().getName()); //set playerturn name
            }
        }

        //Tank collision detected.
        if (model.isTankCollisionDetected()) {
            view.getChildren().remove(view.getCrclProjectile());
            atShotFired.stop();
            view.getChildren().add(view.getVboxVictory());
            if (model.isPlayer1Turn()) {
                //Manage Wins when player 1 wins
                model.getPlayerManager().getPlayer1().upWins();
                System.out.println((model.getPlayerManager().getPlayer1().getName() + ": " + model.getPlayerManager().getPlayer1().getWins() + "Wins"));
                if (model.getPlayerManager().getPlayer1().getWins() == 3) {
                    view.getVboxVictory().getChildren().remove(view.getBtnNextRound());
                    view.getLblName().setText(model.getPlayerManager().getPlayer1().getName() + " won, it took " + model.getPlayerManager().getPlayer1().getTurns() + " turns!");
                    model.getPlayerManager().save(true); //true because player 1 needs to be saved
                } else {
                    view.getLblName().setText(model.getPlayerManager().getPlayer1().getName() + " has " + model.getPlayerManager().getPlayer1().getWins() + " wins");
                }
                bluWinAnimation();

            } else {
                /* Manage Wins when player 2 wins */
                model.getPlayerManager().getPlayer2().upWins();
                System.out.println((model.getPlayerManager().getPlayer2().getName() + ": " + model.getPlayerManager().getPlayer2().getWins() + "Wins"));

                if (model.getPlayerManager().getPlayer2().getWins() == 3) {
                    view.getVboxVictory().getChildren().remove(view.getBtnNextRound());
                    view.getLblName().setText(model.getPlayerManager().getPlayer2().getName() + " won, it took " + model.getPlayerManager().getPlayer2().getTurns() + " turns!");
                    model.getPlayerManager().save(false); //false because player 2 needs to be saved
                } else {
                    view.getLblName().setText(model.getPlayerManager().getPlayer2().getName() + " has " + model.getPlayerManager().getPlayer2().getWins() + " wins");
                }
                ledWinAnimation();
            }
        }

        //Out of bounds collision.
        if (model.isBoundsCollisionDetected()) {
            view.getChildren().remove(view.getCrclProjectile());
            view.getBtnShoot().setDisable(false);
            model.setBoundsCollisionDetected(false);
            atShotFired.stop();
            if (model.isPlayer1Turn()) {
                model.setPlayer1Turn(false);
                view.getLblPlayerTurn().setText(model.getPlayerManager().getPlayer2().getName()); //set playerturn name
            } else {
                model.setPlayer1Turn(true);
                view.getLblPlayerTurn().setText(model.getPlayerManager().getPlayer1().getName()); //set playerturn name
            }
        }
    }


    private void addEventHandlers() {
        view.getBtnShoot().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (model.isPlayer1Turn()) {
                    model.getProjectile().setX(model.getTank1().getX());
                    model.getProjectile().setY(model.getTank1().getY());
                    model.getProjectile().setAngle(view.getSldrAngle().getValue());
                    model.getProjectile().setPower(view.getSldrPower().getValue());
                    view.getIvBluHead().setRotate(-view.getSldrAngle().getValue());

                    //Each time a player shoots, their turns (score) increases by one.
                    model.getPlayerManager().getPlayer1().upTurns();
                    System.out.println(model.getPlayerManager().getPlayer1().getName() + " score is: " + model.getPlayerManager().getPlayer1().getTurns());

                } else {
                    model.getProjectile().setX(model.getTank2().getX());
                    model.getProjectile().setY(model.getTank2().getY());
                    model.getProjectile().setAngle(-view.getSldrAngle().getValue());
                    model.getProjectile().setPower(-view.getSldrPower().getValue());
                    view.getIvLedHead().setRotate(view.getSldrAngle().getValue());

                    //Each time a player shoots, their turns (score) increases by one.
                    model.getPlayerManager().getPlayer2().upTurns();
                    System.out.println(model.getPlayerManager().getPlayer2().getName() + " score is: " + model.getPlayerManager().getPlayer2().getTurns());

                }
                view.getChildren().add(view.getCrclProjectile());
                model.getProjectile().projectileDirections();
                move();
                atShotFired.start();
                view.getSldrAngle().setValue(45);
                view.getSldrPower().setValue(5);
                view.getBtnShoot().setDisable(true);
            }
        });
        view.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    HomeView homeView = new HomeView();
                    Artillery model = new Artillery();
                    HomePresenter presenter = new HomePresenter(homeView, model);
                    view.getScene().setRoot(homeView);
                }
            }
        });
        view.getBtnMainMenu().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                HomeView homeView = new HomeView();
                Artillery model = new Artillery();
                HomePresenter presenter = new HomePresenter(homeView, model);
                view.getScene().setRoot(homeView);
            }
        });
        view.getBtnNextRound().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                GameView gameView = new GameView();
                Artillery model = new Artillery();
                GamePresenter presenter = new GamePresenter(model, gameView);
                view.getScene().setRoot(gameView);
            }
        });
    }
    private void updateView() {
        view.getLblPlayerTurn().setText(model.getPlayerManager().getPlayer1().getName());
        view.getLblScorePl1().setText(model.getPlayerManager().getPlayer1().getName() + ": " + model.getPlayerManager().getPlayer1().getWins());
        view.getLblScorePl2().setText(model.getPlayerManager().getPlayer2().getName() + ": " + model.getPlayerManager().getPlayer2().getWins());

        if (model.getProjectile().getWind().getwX() > 0) {
            view.getIvWindFlag().setRotate(180);
        }

        view.getIvBluHead().setX(model.getTank1().getX() - 50);
        view.getIvBluBody().setX(model.getTank1().getX() - 50);
        view.getIvBluBody().setY(model.getTank1().getY());
        view.getIvBluHead().setY(model.getTank1().getY() - 25);

        view.getIvLedHead().setX(model.getTank2().getX() - 50);
        view.getIvLedBody().setX(model.getTank2().getX() - 50);
        view.getIvLedBody().setY(model.getTank2().getY());
        view.getIvLedHead().setY(model.getTank2().getY() - 25);
    }

}
