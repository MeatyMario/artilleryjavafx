package be.kdg.artillery.views.credit;

import javafx.animation.Interpolator;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.util.Duration;

public class CreditView extends VBox {
    private Label lblThomas;
    private Label lblMario;
    private TranslateTransition ttCredit1, ttCredit2;


    public CreditView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        lblThomas = new Label("Thomas van Sande");
        lblMario = new Label("Mario Mersini");
    }

    private void layoutNodes() {
        //General settings for the Vbox Layout Pane.
        this.setSpacing(20);
        this.setPadding(new Insets(20));
        this.getChildren().addAll(lblThomas, lblMario);
        this.setAlignment(Pos.CENTER);

        //Thomas' label including the animation settings.
        lblThomas.setId("creditLbl");
        lblThomas.setTranslateY(500);
        ttCredit2 = new TranslateTransition();
        ttCredit2.setNode(this.lblThomas);
        ttCredit2.setDuration(Duration.seconds(12));
        ttCredit2.setByY(-1000);
        ttCredit2.setInterpolator(Interpolator.EASE_BOTH);
        ttCredit2.play();

        //Mario's label including the animation settings.
        lblMario.setId("creditLbl");
        lblMario.setTranslateY(500);
        ttCredit1 = new TranslateTransition();
        ttCredit1.setNode(this.lblMario);
        ttCredit1.setDuration(Duration.seconds(12));
        ttCredit1.setByY(-1000);
        ttCredit1.setCycleCount(Timeline.INDEFINITE);
        ttCredit1.setInterpolator(Interpolator.EASE_BOTH);
        ttCredit1.play();

        //Adding the background image.
        BackgroundImage myBI = new BackgroundImage(new Image("homebackground.jpg", 1600, 900, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.setBackground(new Background(myBI));
    }

     TranslateTransition getTtCredit2() {
        return ttCredit2;
    }
}
