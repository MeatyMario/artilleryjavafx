package be.kdg.artillery.views.credit;

import be.kdg.artillery.model.Artillery;
import be.kdg.artillery.views.home.HomePresenter;
import be.kdg.artillery.views.home.HomeView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class CreditPresenter {
    private Artillery model;
    private CreditView view;

    public CreditPresenter(Artillery model, CreditView creditView) {
        this.view = creditView;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getTtCredit2().setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                HomeView homeView = new HomeView();
                Artillery model = new Artillery();
                HomePresenter presenter = new HomePresenter(homeView, model);
                view.getScene().setRoot(homeView);
            }
        });
    }

    private void updateView() {
    }

}
