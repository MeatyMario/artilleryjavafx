package be.kdg.artillery.views.home;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class HomeView extends VBox {
    private Label lblArtillery;
    private Button btnStart;
    private Button btnHighScores;
    private Button btnCredits;
    private Button btnExit;

    public HomeView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        lblArtillery = new Label("Artillery");
        btnStart = new Button("Start");
        btnHighScores = new Button("High Scores");
        btnCredits = new Button("Credits");
        btnExit = new Button("Exit");
    }

    private void layoutNodes() {
        //Lay-out settings for the VBox.
        this.setSpacing(4);
        this.getChildren().addAll(lblArtillery, btnStart, btnHighScores, btnCredits, btnExit);
        this.setAlignment(Pos.CENTER);
        //Style settings for the nodes.
        lblArtillery.setId("artilleryTitle");
        btnStart.setId("homeViewButton");
        btnHighScores.setId("homeViewButton");
        btnCredits.setId("homeViewButton");
        btnExit.setId("homeViewButton");

        //Adding the background image.
        BackgroundImage myBI = new BackgroundImage(new Image("homebackground.jpg", 1600, 900, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.setBackground(new Background(myBI));
    }

    Button getBtnStart() {
        return btnStart;
    }

    Button getBtnHighScores() {
        return btnHighScores;
    }

    Button getBtnCredits() {
        return btnCredits;
    }

    Button getBtnExit() {
        return btnExit;
    }
}
