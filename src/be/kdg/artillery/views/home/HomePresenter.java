package be.kdg.artillery.views.home;

import be.kdg.artillery.model.Artillery;
import be.kdg.artillery.views.credit.CreditPresenter;
import be.kdg.artillery.views.credit.CreditView;
import be.kdg.artillery.views.gamesetup.GameSetupPresenter;
import be.kdg.artillery.views.gamesetup.GameSetupView;
import be.kdg.artillery.views.highscore.HighscorePresenter;
import be.kdg.artillery.views.highscore.HighscoreView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class HomePresenter {
    private HomeView view;
    private Artillery model;


    public HomePresenter(HomeView homeView, Artillery model) {
        this.view = homeView;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getBtnStart().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Artillery artillery = new Artillery();
                GameSetupView gameSetupView = new GameSetupView();
                GameSetupPresenter gameSetupPresenter = new GameSetupPresenter(artillery, gameSetupView);
                view.getScene().setRoot(gameSetupView);
            }
        });

        view.getBtnHighScores().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Artillery artillery = new Artillery();
                HighscoreView highscoreView = new HighscoreView();
                HighscorePresenter highscorePresenter = new HighscorePresenter(artillery, highscoreView);
                view.getScene().setRoot(highscoreView);
            }
        });

        view.getBtnCredits().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CreditView creditView = new CreditView();
                CreditPresenter creditPresenter = new CreditPresenter(model, creditView);
                view.getScene().setRoot(creditView);
            }
        });

        view.getBtnExit().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
    }

    private void updateView() {

    }
}
