package be.kdg.artillery;

import be.kdg.artillery.model.Artillery;
import be.kdg.artillery.views.home.HomeView;
import be.kdg.artillery.views.home.HomePresenter;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HomeView homeView = new HomeView();
        Artillery model = new Artillery();
        HomePresenter presenter = new HomePresenter(homeView, model);
        Scene scene = new Scene(homeView);
        scene.getStylesheets().add("/stylesheets/artillery.css");
        primaryStage.setScene(scene);
        primaryStage.setTitle("Battle of the Chuies");
        primaryStage.setWidth(1400);
        primaryStage.setHeight(800);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("icon.png"));
        primaryStage.show();
    }

}

