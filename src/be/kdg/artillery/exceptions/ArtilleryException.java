package be.kdg.artillery.exceptions;

public class ArtilleryException extends RuntimeException {
    public ArtilleryException() {
    }

    public ArtilleryException(String message) {
        super(message);
    }

    public ArtilleryException(String message, Throwable cause) {
        super(message, cause);
    }

    public ArtilleryException(Throwable cause) {
        super(cause);
    }

    public ArtilleryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
