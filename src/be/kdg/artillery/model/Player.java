package be.kdg.artillery.model;

import java.time.LocalDate;

public class Player implements Comparable<Player>{
    private String name;
    private LocalDate date;
    private int wins; //Wins are the amount of times that player hit A TANK. (When the player has 3, he wins the game)
    private int turns; //Turns are the amount of time the player has shot.

    public Player(String name, int score, LocalDate date) {
        this.name = name;
        this.date = date;
        this.turns = score;
        this.wins = 0;
    }

    public void upTurns(){
        this.turns = turns + 1;
    }
    public void upWins(){
        this.wins = wins + 1;
    }

    public String getName() {
        return name;
    }
    public LocalDate getDate() {
        return date;
    }
    public int getWins() {
        return wins;
    }
    public int getTurns() {
        return turns;
    }

    //CompareTO method: used to sort the Playerlist by turns Ascending
    @Override
    public int compareTo(Player otherPlayer) {
        return this.turns - otherPlayer.turns;
    }
}