package be.kdg.artillery.model;

//Just a simple class to store tank x & y coordinates.
public class Tank {

    //Properties
    private double x;
    private double y;

    //Constructors
    public Tank(double x, double y) {
        this.x = x;
        this.y = y;
    }

    //Methods
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
