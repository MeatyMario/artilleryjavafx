package be.kdg.artillery.model;

import java.util.Random;

/*
This class contains all the necessary information for the projectile. It also contains the wind as an inner class.
*/

public class Projectile {
    public static class Wind {
        Random random;
        private double wX;
        //If wX is positive, the wind blows to the left, if it is negative it blows to the right.
        //Constructors
        Wind() {
            this.wX = 0;
            boolean wDirection;
            random = new Random();
            wX = (random.nextInt(5) + 8);
            wDirection = random.nextBoolean(); // This boolean decides the wind direction, if wDirection is positive it blows to the right. If it is negative, it blows to the left.
            if (!wDirection) {
                wX = -wX;
            }
        }

        public double getwX() {
            return wX / 1000;
        }
    }

    private Wind wind;
    private double x;
    private double y;
    private double vx;
    private double vy;
    private double angle;
    private double power;
    private final double GY = 0.075;

    public Projectile() {
        this.wind = new Wind();
    }

    public void projectileDirections() {
        //Mathematical formulas for the directions (angle and power).
        vx = Math.cos(Math.toRadians(angle)) * power;
        vy = Math.sin(Math.toRadians(angle)) * power;
    }

    public void move() {
        //This method is the main method to move a projectile and applies gravity and wind forces.
        this.vx = this.vx - wind.getwX();
        this.vy = this.vy - GY;
        this.x = this.x + this.vx;
        this.y = this.y - this.vy;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public Wind getWind() {
        return wind;
    }
}
