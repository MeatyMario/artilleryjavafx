package be.kdg.artillery.model;

/* This simple class contains a X and Y value. These points will be used to create the terrain. */
public class Point {
    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
