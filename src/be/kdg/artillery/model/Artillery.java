package be.kdg.artillery.model;

import java.util.Comparator;

/* Made by Thomas van Sande & Mario Mersini.
The Artillery Class is our model class and ensures a good connection between the presenters and the other classes, aswel
as containing several methods itself such as the collisions. It also takes care of the turns.

* */
public class Artillery {
    //Properties
    private Projectile projectile;
    private Terrain terrain;
    private Tank tank1, tank2;
    private PlayerManager playerManager;
    private boolean terrainCollisionDetected, boundsCollisionDetected, tankCollisionDetected;
    private boolean player1Turn = true;


    //Constructors
    public Artillery() {
        projectile = new Projectile();
        playerManager = new PlayerManager();
        terrain = new Terrain();
        tank1 = new Tank(terrain.getTerrainCoordinates().get(1).getX(), terrain.getTerrainCoordinates().get(1).getY() - 75);
        tank2 = new Tank(terrain.getTerrainCoordinates().get(9).getX(), terrain.getTerrainCoordinates().get(9).getY() - 75);
    }

    //Methods
    /* In this method, we will check for collisions and general and update a boolean if something is deemed to be true. */
    public void checkCollisions() {
        projectile.move();
        /*Tank Collision.
         If X & Y of projectile are same as Tank X & Y, boolean tankCollisionDetected becomes true. 50 and 30 numbers are being used as bufferzones around tank. */
        if (player1Turn) {
            if (projectile.getX() > tank2.getX() - 50 && projectile.getX() < tank2.getX() + 30 && projectile.getY() > tank2.getY()) {
                tankCollisionDetected = true;
            }
        } else {
            if (projectile.getX() > tank1.getX() - 30 && projectile.getX() < tank1.getX() + 50 && projectile.getY() > tank1.getY()) {
                tankCollisionDetected = true;
            }
        }

        /* Out of bounds collision.
        If the X value of the projectile is outside the screen, boolean boundsCollisionDetected becomes true. */
        if (projectile.getX() >= 1375 || projectile.getX() <= 25) {
            boundsCollisionDetected = true;
        }

        /* Terrain collision.
        The terrain is generated in pieces of 140 pixels. Each 140 pixels a new point is generated in the Terrain Class.
        While the projectile is travelling over a certain piece of terrain it will select the X,Y coordinates of both
        points to the left and right of the projectile. Using the right points, the formula will find the tY value
        (height of terrain at X position of projectile). Then at the end we check if we substract the values and it becomes
        0 or less, it will detect a terrain collision.
         */
        double x1 = 0;
        double y1 = 0;
        double x2 = 0;
        double y2 = 0;
        double terrainY;
        for (int i = 0; i < terrain.getTerrainCoordinates().size(); i++) {
            if (projectile.getX() > terrain.getTerrainCoordinates().get(i).getX() && projectile.getX() <= terrain.getTerrainCoordinates().get(i + 1).getX()) {
                x1 = terrain.getTerrainCoordinates().get(i).getX();
                y1 = terrain.getTerrainCoordinates().get(i).getY();
                x2 = terrain.getTerrainCoordinates().get(i + 1).getX();
                y2 = terrain.getTerrainCoordinates().get(i + 1).getY();
            }
        }
        terrainY = (((projectile.getX() - x1) * (y2 - y1)) / (x2 - x1) + y1);
        if (terrainY - projectile.getY() <= 0) {
            terrainCollisionDetected = true;
            /* Terrain Deformation.
            Then we get the list of terrain coordinates and we add a new point in there containing
            that last known position of the projectile and the ty value we had above added with a value of 25 to create a dent
            in the terrain. Finally the arraylist gets sorted based on the X values to make sure that the deformation is at the
            right position on the terrain.
            */
            getTerrain().getTerrainCoordinates().add(new Point(projectile.getX(), terrainY + 25));
            getTerrain().getTerrainCoordinates().sort(new Comparator<Point>() {
                @Override
                public int compare(Point p1, Point p2) {
                    return (int) (p1.getX() - p2.getX());
                }
            });
        }
    }


    public Tank getTank1() {
        return tank1;
    }

    public Tank getTank2() {
        return tank2;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public Projectile getProjectile() {
        return projectile;
    }

    public boolean isPlayer1Turn() {
        return player1Turn;
    }

    public void setPlayer1Turn(boolean player1Turn) {
        this.player1Turn = player1Turn;
    }

    public boolean isTerrainCollisionDetected() {
        return terrainCollisionDetected;
    }

    public void setTerrainCollisionDetected(boolean terrainCollisionDetected) {
        this.terrainCollisionDetected = terrainCollisionDetected;
    }

    public boolean isBoundsCollisionDetected() {
        return boundsCollisionDetected;
    }

    public void setBoundsCollisionDetected(boolean boundsCollisionDetected) {
        this.boundsCollisionDetected = boundsCollisionDetected;
    }

    public boolean isTankCollisionDetected() {
        return tankCollisionDetected;
    }

    public void setTankCollisionDetected(boolean tankCollisionDetected) {
        this.tankCollisionDetected = tankCollisionDetected;
    }
}