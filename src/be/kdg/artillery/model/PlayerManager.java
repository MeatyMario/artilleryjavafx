package be.kdg.artillery.model;

import be.kdg.artillery.exceptions.ArtilleryException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

public class PlayerManager {
    private List<Player> playerList;
    private static Player player1;
    private static Player player2;

    PlayerManager() {
        this.playerList = new ArrayList<>();
    }

    /*creates 2 players and gives them a temporary score of 0(will be filled in when they get saved)*/
    public void createPlayers(String name1, String name2) {
        player1 = new Player(name1, 0, LocalDate.now());
        player2 = new Player(name2, 0, LocalDate.now());
    }

    /*The parameter is a boolean because there are only 2 options, Player1 needs to be saved(true)
    or Player2 needs to be saved(false).*/
    public void save(boolean playerOneSave) {
        if (playerOneSave) {
            playerList.add(new Player(player1.getName(), player1.getTurns(), player1.getDate()));
        } else playerList.add(new Player(player2.getName(), player2.getTurns(), player2.getDate()));

        load(); //Reading the existing file and making new Players (who also get added to the playerlist)

        /*the playerlist is now filled with the old players (because we loaded them)
         and 1 new Player (the player that won the game).*/
        try (Formatter fm = new Formatter("resources/highscores.sav")) {
            for (Player player : playerList) {
                fm.format("%s;%d;%s\n", player.getName(), player.getTurns(), player.getDate()); //Writes for each player: name;score;date.
            }
        } catch (IOException e) {
            throw new ArtilleryException("Problem while saving!", e);
        }
    }

    public void load() {
        Path pathtoHighscoresFile = Paths.get("resources/highscores.sav"); //Path to highscores file
        if (Files.exists(pathtoHighscoresFile)) { //Testing if the file exists.
            try (Scanner s = new Scanner(new File("resources/highscores.sav"))) {
                while (s.hasNext()) { //For each line in the highscores.sav do:
                    String oneLine = s.nextLine(); //A line looks like: Thomas;4;2020-03-17.
                    String[] strings = oneLine.split(";");
                    /*strings[0] ="Thomas"
                      strings[1] = 4
                      strings[2] = 2020-03-17*/
                    LocalDate tempDate = LocalDate.parse(strings[2]); //Parsing the date from a String to a LocalDate.
                    playerList.add(new Player(strings[0], Integer.parseInt(strings[1]), tempDate));
                }
            } catch (IOException ioexception) {
                throw new ArtilleryException("Problem while reading the file!", ioexception);
            } finally {
                Collections.sort(playerList); //Always sort the list
            }
        }
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }
}
