package be.kdg.artillery.model;

import java.util.*;

//This is the class where the terrain and biome is being made.
public class Terrain {
    private List<Point> terrainCoordinates;
    private int biome;
    private Random random = new Random();

    public Terrain() {
        this.terrainCoordinates = new ArrayList<Point>();
        generateTerrain();
    }

    //Methods
    public void generateTerrain() {
        /* Here the Points get added to an arraylist(terraincoordinates).
        The reason this is done seperately and not in a loop is because the different points have different
        bounds and starting values. This is done to create a smoother terrain with a bump in the middle and flatter outsides.
        If they were all to be added in a loop with the same values the terrain will be very jagged and there will usually
        be no mountain in the middle.
        */
        terrainCoordinates.add(new Point(0, random.nextInt(100) + 500));
        terrainCoordinates.add(new Point(140, random.nextInt(100) + 500));
        terrainCoordinates.add(new Point(280, random.nextInt(100) + 500));
        terrainCoordinates.add(new Point(420,random.nextInt(200) + 300));
        terrainCoordinates.add(new Point(560, random.nextInt(200) + 200));
        terrainCoordinates.add(new Point(700, random.nextInt(200) + 100));
        terrainCoordinates.add(new Point(840, random.nextInt(200) + 200));
        terrainCoordinates.add(new Point(980, random.nextInt(200) + 300));
        terrainCoordinates.add(new Point(1120, random.nextInt(100) + 500));
        terrainCoordinates.add(new Point(1260, random.nextInt(100) + 500));
        terrainCoordinates.add(new Point(1400, random.nextInt(100) + 500));
    }

    public void generateBiome() {
        //Biome gets generated. 1: Hillside, 2: Wild West/Desert, 3: Snowy/North Pole.
        biome = 2;
    }


    public int getBiome() {
        return biome;
    }

    public List<Point> getTerrainCoordinates() {
        return terrainCoordinates;
    }
}